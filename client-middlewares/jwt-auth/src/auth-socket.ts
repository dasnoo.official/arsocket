import { AuthEvent } from './auth-event.interface';
import { Observable } from 'rxjs/Observable';
import { Socket } from '@dasnoo/arsocket-client';
import { SocketOpts } from "@dasnoo/arsocket-client";
import { ISocket } from "@dasnoo/arsocket-client";

// This class is a wrapper on top of socket to
// use when using an authentication service like
// auth0. 
// You must provide an observable that tells this
// class when an authentication event happens
export class AuthSocket extends Socket implements ISocket{

  private isAuthenticatedOnServer: boolean;
  private expirity: number;
  private token: string | undefined;

  constructor(){
    super();
    this.select('auth')
        .subscribe(r => this.onAuth(r));
  }

  setAuthObs(options: SocketOpts = {}, authObs?: Observable<AuthEvent>){
    if(authObs)
      authObs.subscribe(authEvent => this.onAuthEvent(authEvent));
    else
      console.warn('No auth observable found on AuthSocket initialization.');
  }

  private onAuthEvent(authEvent: AuthEvent){
    // nothing to do here. It might happen on initialization because
    // we are using a BehaviorSubject.
    if(!authEvent.token)
      return;
    if(authEvent.auth)
      this.sendAuthentication(authEvent);
    else
      this.sendLogout();
  }

  send(type: string, payload?: any, metadata: any = {}){
    if(!this.isAuthenticatedOnServer && this.token){
      // If we are not authenticated on the server but we are on the client 
      // we send the JWT with the request.
      // might happen when we send a request prior to getting the authentication
      // response.
      metadata.auth = { token: this.token };
    }
    return super.send(type, payload, metadata);
  }

  private sendAuthentication(authEvent: AuthEvent){
    // we send an authentication request to the server which is gonna authenticate
    // the websocket connection server side.
    this.token = authEvent.token;
    this.send('AUTH');
  }

  private onAuth(exp: number){
    this.isAuthenticatedOnServer = true;
    this.expirity = exp; 
  }

  private sendLogout(){
    this.send('LOGOUT');
    // no need to subscribe here we don't really care about the response
    this.isAuthenticatedOnServer = false;
    this.expirity = 0;
    this.token = undefined;
  }

  // when the expirity is about to expire it's user job to issue another AuthEvent..
  get authenticated(){
    return this.isAuthenticatedOnServer && this.expirity > Date.now();
  }
}
