export interface AuthEvent{
	// whether the event is sign in (true) or logout (false)
	auth: boolean;
	// token as string
	token: string;
	// expiration as timestamp
	exp: number; 
}