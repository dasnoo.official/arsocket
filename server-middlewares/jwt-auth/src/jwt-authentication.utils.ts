import { Subject } from 'rxjs/Subject';
import * as jsonwebtoken from 'jsonwebtoken';
import { ActionEvent } from "@dasnoo/arsocket-server";
import { Connection, log } from "@dasnoo/arsocket-server";

// This function verifies the token and if verified, adds info to the connection auth object.
//
// Since this is trying to comply with many use case, it's not perfect, thus you are
// encouraged to implement your own authentication middleware.

// Take the case where the client authenticate with an external service like Auth0.
// The server doesn't know yet that the user has been issued a JWT.
// One could send a request to the server to authenticate the connection.
// Alternatively one could make a request for something else //
// and send the token as a metadata so the server authenticates 
// the connection with said metadata with a middleware

// So far so good. The problem arise if the client makes multiple request simultaneously (or almost)
// Let's call those R1 and R2.
// Thus the client sends the jwt token in each request.
// There is no need to authenticate the user twice so we need to check that the previous
// token is not the same as the new one. In other word the server will process R1. Then we
// need to check that R2 token is not the same as R1 token. Well but this function is async.
// thus here lies the difficulty.

// There are ways around this but the way the design chosen work is that in the event that 
// R1 callback has not been proccessed when r2 starts to be proccessed, 
// then the verifyToken method will be running twice.
// This shouldn't be an issue though as the verifyToken function isn't intensive
// & the time window for this to happen is very short. 

export function JwtAuthentication(publicKey: string, opts: any, authSubject?: Subject<Connection>): Function{
	return async ({ connection, action }: ActionEvent) => {
		// checking that the info's are there. If not then we return true
		const meta = action.metadata || {};
		const authEvt = meta.authEvent || {};
		const token = authEvt.token;
		const auth = connection.auth;
		const lastToken = auth.tokenStr || '';
		// always return true, authentication middleware always succeeds 
		// whether user is authenticated or not
		// authorizasition middleware should do the job of authorizing
		if(!token)
			return true;
		// check to see if the connection has already been authenticated
		// with the same token
		if(auth.authenticated && lastToken === token)
			return true;

		try{
			const decoded = await verifyToken(token, publicKey, opts);
			await addUserInfo(connection, decoded, token);
			await pushEvt(connection, token, lastToken, authSubject);
		}catch(e){
			log.debug(e);
		}
		return true;
	}
}

// add user info to connection.auth object 
function addUserInfo(connection: Connection, tokenPayload: any, tokenStr: string){
	if(tokenPayload){
		const auth = connection.auth;
		auth.token = tokenPayload;
		auth.exp = tokenPayload.exp; 
		auth.tokenStr = tokenStr; 
		Object.defineProperty(auth, 'authenticated', {
			get: function() { return (auth.exp || 0) > (Date.now() / 1000); }
		});
	}
	return tokenPayload;
}


function verifyToken(token: any, publicKey: string, opts: any): Promise<any>{
	return new Promise((resolve, reject) => {
		jsonwebtoken.verify(token, publicKey, opts,  (err, decoded) => {
			if(err){
				resolve(undefined);
			} 
			else {
				resolve(decoded);
			}
		});
	});
}

// push event to subject. Makes sure we don't post the same event twice by checking that the
// JWT string aren't equal.
function pushEvt(connection: Connection, token: string, lastToken: string,
								authSubject?: Subject<Connection>){
	if(authSubject && token === lastToken)
		 authSubject.next(connection)
}