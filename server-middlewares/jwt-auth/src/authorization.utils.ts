import { Connection } from '@dasnoo/arsocket-server';
import { ActionEvent } from "@dasnoo/arsocket-server";


// authorize action when user has a specific role..
export function hasRole(role: string): Function{
	return function(a: ActionEvent){
		return a.connection.auth.authenticated 
						&& a.connection.auth.role === role;
	};
}
