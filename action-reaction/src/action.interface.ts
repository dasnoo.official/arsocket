
export interface Action<T> {
	id?: number;
	type: string;
	payload?: T;
	metadata?: any;
	timestamp?: number;
	hash?: number;
}