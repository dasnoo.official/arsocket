let AR = require('../socket/dist/index');

const socket = new AR.Socket();

socket.init({host: 'localhost', port: 3000, path: 'arsocket', engine: 'websocket' });
// getting dom elems
let btn = document.getElementById("btn");
let inp =  document.getElementById("inp");
let res = document.getElementById("res");
let hi = document.getElementById("hi");

btn.addEventListener("click", e => {
	console.log("clicked new_message")
	socket.send("NEW_MESSAGE", inp.value, true).subscribe(r => console.log("r"));
});

hi.addEventListener("click", e => {
	console.log("clicked hi")
	socket.send("JOIN");
});


socket.select('NEW_MESSAGE').subscribe(m => {
	console.log('received')
	res.innerHTML = res.innerHTML + `${m}<br>`
});

// socket.select('USERS').subscribe(m => {
// 	room1.innerHTML = m;
// })