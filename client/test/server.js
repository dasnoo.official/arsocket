var express = require('express');
var app = express();
var http = require('http').createServer(app);
var sockjs = require('sockjs');

app.use('/', express.static("./"));

http.listen(3000, function(){
  console.log('listening on *:3000');
});
    
var socket = sockjs.createServer({ sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js' });
socket.installHandlers(http, {prefix:'/arsocket'});

socket.on('connection', (connection) => {
  console.log('a user connected');
  connection.on('data', function(msg){
    console.log(msg);
  });
});


