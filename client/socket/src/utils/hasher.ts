export class Hasher{
	
	static hash(timestamp = 0){
		const str = "!"  + timestamp;
    const length = str.length;
		let hash = 0;
    if (length === 0) return hash;
    for (let i = 0; i < length; i++) {
      const char = str.charCodeAt(i);
      hash = ((hash<<5)-hash)+char;
      hash = hash & hash; 
    }
    return hash;
	}
}