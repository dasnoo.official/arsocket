import { SocketOpts } from './../options/socket-options.interface';

export class Throttler{
	timeout: number;
	cap: number;
	tries = 0;

	constructor({ reconnectionDelay = 200, reconnectionDelayMax = 5000 }:SocketOpts = {}){
		this.timeout = reconnectionDelay as number;
		this.cap = reconnectionDelayMax as number;
	}

	get throttleTime(): number{
		// we increase the timeout and cap it at 5 sec
		let t = (this.timeout * this.tries * this.tries++);
		if(t > this.cap)
			t = this.cap;
		return t;
	}
}