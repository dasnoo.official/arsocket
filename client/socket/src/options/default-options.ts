import { SocketOpts } from "./socket-options.interface";
import { Engines } from "./engines";

export const DEFAULT_OPTS_CLIENT: SocketOpts = {
	host: 'localhost', 
	port: 3000, 
	path: 'arsocket',
	secure: false, 
	engine: Engines.websocket,
	reconnectionDelay: 200,
	reconnectionDelayMax: 5000
};

