import { Observable } from 'rxjs/Observable';
import { Engines } from "./engines";

export interface SocketOpts {
	host?: string, 
	port?: number, 
	path?: string,
	engine?: Engines,
	secure?: boolean,
	onOpen?: Function,
	reconnectionDelay?: number;
	reconnectionDelayMax?: number;
}
