import { DEFAULT_OPTS_CLIENT } from './default-options';
import { SocketOpts } from "./socket-options.interface";

// responsible to guarantee that socket will start whatever option is passed
// into Socket (to a certain extent).
export class OptionBuilder {

	static build(opts: SocketOpts) {
		if(opts.path)
			opts.path = OptionBuilder.formatPath(opts.path);
		const temp = Object.assign({}, DEFAULT_OPTS_CLIENT);
		return Object.assign(temp, opts);
	}
	// format leading space and /, same for end of str
	private static formatPath(str: string){
		return str.replace(/^[\s/]*|[\s/]*$/g, '');
	}

	private static checkHostValidity(host: string){
		if(~host.indexOf("/"))
			throw new Error(`the host should not contain the protocol, 
			not the char '/' Example: localhost instead of http://localhost.
			the correct protocol is automatically added for you. If you need to use
			a secure server then use the secure: true option (for wss and https)`);
		if(~host.indexOf(":"))
			throw new Error(`the host should not contain the port, not the char ':' 
			Example: localhost instead of localhost:3000.
			Use the port option to specify the port`);
	}
}

