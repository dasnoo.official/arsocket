export enum Engines{
	websocket = 'websocket',
	engineIO = "engine.io",
	socketIO = "socket.io",
	sockjs = "sockjs"
}