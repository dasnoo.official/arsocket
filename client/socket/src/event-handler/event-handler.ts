import { SocketOpts } from './../options/socket-options.interface';
import { Throttler } from './../utils/throttler';
import { Bridge } from "../bridges/bridge";

export class EventHandler {

	constructor(private options: SocketOpts,
							private bridge:Bridge, 
							private throttler: Throttler,
							private selectionMap: { [key:string]: any }){}
	
	connect(){
    const socket = this.bridge.init(this.options);
    this.bridge.onOpen(() => this.onOpen());
    this.bridge.onData((str: string) => this.onMessage(str));
    this.bridge.onClose(() => this.tryConnect());
    // some impl will sends error & close event on failed to connect
    // (most impl, really), thus if we use the error as well it breaks.
		// Bridge.onError(e => { console.log("Error: " + e); this.tryConnect(); } );
		return socket;
	}
	
  private tryConnect() {
    setTimeout(_ => this.connect(), this.throttler.throttleTime);
  }

	private onMessage(actionstr: string){
    const action = JSON.parse(actionstr);
    const r = this.selectionMap[action.type];
    if(r)
      r.subject.next(action.payload);
  }

  private onOpen(){
    this.throttler.tries = 0;
    if(typeof this.options.onOpen === 'function')
      this.options.onOpen();
  }

}