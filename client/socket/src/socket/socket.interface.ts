
import { SocketOpts, Socket } from "../index";
import { Observable } from "rxjs/Observable";

export interface ISocket {
  readonly socket: any;

  init:(options: SocketOpts) => Socket | undefined;

  send:(type: string, payload?: any, metadata?: {}) => Observable<any>;

  select:(type: string) => Observable<any>

}