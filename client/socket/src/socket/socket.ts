import { ISocket } from './socket.interface';
import { EventHandler } from './../event-handler/event-handler';
import { Throttler } from './../utils/throttler';
import { DEFAULT_OPTS_CLIENT } from '../options/default-options';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Bridge } from "../bridges/bridge";
import { Action } from "@dasnoo/action-reaction";
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';

import { SocketOpts } from "../options/socket-options.interface";
import { OptionBuilder } from "../options/options-builder";
import { Hasher } from "../utils/hasher";


export class Socket implements ISocket{
  private _socket: any;
  private selectionMap: { [key:string]: any } = {};
  private throttler: Throttler;
  private options: SocketOpts;
  private eventHandler: EventHandler;
  private bridge: Bridge;
  private actionID = 0;

  constructor(){}

  init(options: SocketOpts = {}): Socket | undefined{
    if(! window)
      return;
    this.options = OptionBuilder.build(options);
    this.bridge = new Bridge(this.options.engine as string);
    this.throttler = new Throttler(this.options);
    this.eventHandler = new EventHandler(this.options, this.bridge, 
      this.throttler, this.selectionMap);
    this._socket = this.eventHandler.connect()
    return this;
  }

  send(type: string, payload?: any, metadata: {} = {}):Observable<any>{
    const timestamp = Date.now();
    const hash = Hasher.hash(timestamp);
    const id = this.actionID++;
    const action: Action<any> = { type, payload, timestamp, hash, id};
    Object.assign(action, metadata);
    this.trySend(action);
    return this.select(action.type)
      .filter( reaction => reaction.id === this.actionID - 1 )
      .take(1);
  }

  private trySend(action: Action<any>){
    // if the socket isn't ready we wait
    Observable
      .of(this._socket)
      .do( sock => {
        if(!this.bridge.isReady())
          throw new Error('not ready');
      })
      .retryWhen( err => {
        return err.delay(this.throttler.throttleTime);
      })
      .take(1)
      .subscribe( sock => this.bridge.send(action));
  }

  // First filter was used here
  // Using a map might be better though.
  select(type: string): Observable<any> {
    this.addToMap(type);
    return this.selectionMap[type].observable;
  }

  private addToMap(type: string) {
    if(this.selectionMap[type]) 
      return;
    const subject = new Subject<any>();
    const observable = subject.asObservable();
    this.selectionMap[type] = { subject, observable };  
  }

  get socket(){
    return this._socket;
  }

}