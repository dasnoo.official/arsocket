import { EngineIOBridge } from './engine.io.bridge';
import { WebsocketBridge } from './websocket.bridge';
import { SockJSBridge } from './sockjs.bridge';
import { IBridge } from './bridge.interface';
import { SocketIOBridge } from './socket.io.bridge';
import { Action } from "@dasnoo/action-reaction";
import { SocketOpts } from "../index";

export class Bridge{
	bridge: IBridge;

	constructor(engine: string){
		this.setEngine(engine);
	}

	private setEngine(engine: string){
		switch(engine){
			case "socket.io":
				console.warn('arsocket : using socket.io engine is not recommended, use engine.io instead')
				this.bridge = new SocketIOBridge(); break;
			case "sockjs" :
				this.bridge = new SockJSBridge(); break;
			case "websocket":
				this.bridge = new WebsocketBridge(); break;
			case "engine.io":
				this.bridge = new EngineIOBridge(); break;
			default:
				throw Error(`${engine} hasn't been implemented yet.`);
		}
	}

	init(options: SocketOpts){
		return this.bridge.init(options);
	}

	onOpen(fn: Function){
		return this.bridge.onOpen(fn);
	}
	
	send(action: Action<any>){
		return this.bridge.send(action);
	}

	onData(fn: Function){
	 return this.bridge.onData(fn);
	}

	onClose(fn: Function){
		return this.bridge.onClose(fn);
	}
	
	onError(fn: Function){
		return this.bridge.onError(fn);
	}

	isReady():boolean{
		if(! this.bridge.socket)
			return false;
		return this.bridge.isReady();
	}

	close(){
		if(! this.bridge.socket)
			return;
		return this.bridge.close()
	}
}