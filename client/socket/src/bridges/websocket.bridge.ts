import { IBridge } from './bridge.interface';
import { Action } from "@dasnoo/action-reaction";
import { SocketOpts } from "../index";


export class WebsocketBridge implements IBridge{
	socket: any;

	init({ host, port, path, secure }: SocketOpts) {
		let protocol = secure ? 'wss': 'ws';
		port = (port === undefined ? 80 : port);
		this.socket = new WebSocket(`${protocol}://${host}:${port}/${path}`);
		return this.socket;
	}

	send(action: Action<any>){
		this.socket.send(JSON.stringify(action));
	}

	onOpen(fn: Function){
		this.socket.onOpen = fn;
	}

	onData(fn: Function){
		let wrap = (e: MessageEvent) => {
			fn(e.data);
		};
		this.socket.onmessage = wrap;
	}

	onClose(fn: Function){
		this.socket.onclose = fn;
	}

	isReady():boolean{
		return this.socket.readyState === 1;
	}

	onError(fn: Function){
		this.socket.onerror = fn;
	}
	
	close(){
		this.socket.close();
	}
}