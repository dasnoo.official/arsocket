import { Action } from '@dasnoo/action-reaction';
import { IBridge } from './bridge.interface';
import { EngineIOBridge } from "./engine.io.bridge";
import { SocketOpts } from "../options/socket-options.interface";

declare var io:any;

export class SocketIOBridge extends EngineIOBridge implements IBridge{
	socket: any;

	init(options: SocketOpts) {
		console.warn('using socket.io engine is not advised. Use engine.io instead');
		const protocol = options.secure ? 'https': 'http';
		const port = options.port === undefined ? `${options.port}` : '80';
		const url = `${protocol}://${options.host}:${port}/${options.path}`;
		this.socket = io(url, { rememberUpgrade : true });
		return this.socket;
	}

	send(action: Action<any>) {
		this.socket.emit('', action);
	}

}