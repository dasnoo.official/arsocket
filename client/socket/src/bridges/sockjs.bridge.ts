import { WebsocketBridge } from './websocket.bridge';
import { Action } from "@dasnoo/action-reaction";
import { SocketOpts } from "../index";

//client must add lib in head
declare var SockJS: any;

export class SockJSBridge extends WebsocketBridge{
	socket: any;

	init(opts: SocketOpts) {
		let protocol = opts.secure ? 'https': 'http';
		opts.port = (opts.port === undefined ? 80 : opts.port);
		this.socket = new SockJS(`${protocol}://${opts.host}:${opts.port}/${opts.path}`);
		return this.socket;
	}
	
}