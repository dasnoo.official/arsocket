import { WebsocketBridge } from './websocket.bridge';
import { Action } from '@dasnoo/action-reaction';
import { IBridge } from './bridge.interface';
import { SocketOpts } from "../index";
declare var eio: any;

export class EngineIOBridge implements IBridge{
	socket: any;

	init(options: SocketOpts) {
		const eopts:any = {};
		eopts.path = `/${options.path}`;
		eopts.host = options.host;
		eopts.port = options.port;
		this.socket = eio.Socket(eopts);
		return this.socket;
	}
	send(action: Action<any>) {
		this.socket.send(action);
	}

	onOpen(fn: Function){
		this.socket.on('open', fn);
	}

	onData(fn: Function){
		this.socket.on('message', fn);	
	}

	onClose(fn: Function){
		this.socket.on('close', fn);
	}

	onError(fn: Function){
		this.socket.on('error', fn);
	}

	isReady(){
		return this.socket.readyState === 'open';
	}

	close(){
		
	}
}