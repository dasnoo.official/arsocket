import { Action } from '@dasnoo/action-reaction';
import { SocketOpts } from "../index";

export interface IBridge {
	socket: any;
	init: (options: SocketOpts) => any;
	send: (action: Action<any>) => any
	onOpen: (fn: Function) => any
	onData: (fn: Function) => any;
	onClose: (fn: Function) => any;
	onError: (fn: Function) => any;
	isReady: () => boolean;
	close: () => any;
}