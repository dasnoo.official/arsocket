import { Connection } from './connection.interface';
import { Action } from '@dasnoo/action-reaction';

export interface ActionEvent{
	connection: Connection;
	action: Action<any>;
}

