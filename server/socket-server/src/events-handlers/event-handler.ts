import { AntiSpam } from './../utils/anti-spam';
import { Bridge } from './../bridges/bridge';
import { ActionEvent } from './action-event.interface';
import { Observable } from 'rxjs/Observable';
import { log } from './../utils/logger';
import { ErrorEvent } from './error-event.interface';
import { Connection, Auth } from './connection.interface';
import { Subject } from "rxjs/Subject";
import { Action, Reaction } from '@dasnoo/action-reaction';


// Take events, and push it into observables.
// Make some adjustement as well
export class SocketEventHandler{
	// subject for pushing events
	private _connection = new Subject<Connection>();
	private _action = new Subject<ActionEvent>();
	private _error = new Subject<ErrorEvent>();
	private _close = new Subject<Connection>();
	private _reactions = new Subject<Reaction<any>>();
	
	// it's better to expose observables instead of subjects.
	public connection$ = this._connection.asObservable();
	public action$ = this._action.asObservable();
	public error$ = this._error.asObservable();
	public close$ = this._close.asObservable();
	public reaction$ = this._reactions.asObservable();
	public connectionID = 0;

	constructor(private bridge: Bridge){
		this.addEventHandlers();
	}

	private addEventHandlers(){
		this.bridge.onConnection((engineConnection: any) => {
			// wrapping the connection from the underlying engine & adding auth
			const auth: Auth = {};
			const metadata = {};
			// 2 billion for v8 optimisation for 31 bts integer, not sure if relevant here
			const connectionID = this.connectionID++ % 2000000000;
			const connection: Connection = { auth, connectionID, engineConnection, metadata };
			this.bridge.onError(connection, (e: Error) => this.onError(e, connection));
			this.bridge.onClose(connection, () => this.onClose(connection));
			this.bridge.onAction(connection, (str: string) => this.onAction(str, connection));
			this._connection.next(connection);
			log.debug(`connection opened, connection id: ${connection.connectionID}`);
		});
	}

	private onClose(connection: Connection){
		this._close.next(connection);
		log.debug(`connection closed, connection id: ${connection.connectionID}`);
	}

	private onError(error: Error, connection: Connection){
		this._error.next({connection, error});
		log.error(error.message);
	}

	// when a message is received from the client
	private onAction(actionstr: string, connection: Connection){
		const action: Action<any> = JSON.parse(actionstr)
		// do nothing if "spam" 
		if(AntiSpam.isSpam(action)){
			log.warn("Tentative of sending message without client. Forgetting message.");
			return;
		}
		log.debug(`Action received: \n ${JSON.stringify(action)}`.grey);
		this._action.next({action, connection});
	}

	addReaction(r: Reaction<any>, c: Connection){
		this.bridge.send(c, r);	
		this._reactions.next(r);
		log.debug(`Reaction: \n ${JSON.stringify(r)}`.grey);
	}
}

