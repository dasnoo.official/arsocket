import { Action } from "@dasnoo/action-reaction";

export interface Connection {
	connectionID: number;
	auth: Auth;
	engineConnection: any;
	rooms?: Map<string, any>;
	metadata?: any;
}

export interface Auth{
  token?: any;
  tokenStr?: string;
  exp?: number;
  id?: string | number;
  userID?: string | number;
  user?: any;
	role?: string;
  authenticated?: boolean;
}


