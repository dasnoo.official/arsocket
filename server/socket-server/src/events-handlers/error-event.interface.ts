import { Connection } from './connection.interface';

export interface ErrorEvent{
	connection: Connection;
	error: Error;
}