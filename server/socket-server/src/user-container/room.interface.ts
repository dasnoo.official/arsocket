import { Connection } from "../index";

export interface Room extends Map<number, Connection> { }
export interface Rooms extends Map<string, Room> { }