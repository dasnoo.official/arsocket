import { Room, Rooms } from './room.interface';
import { log } from './../utils/logger';
import { Connection } from './../events-handlers/connection.interface';
import { SocketEventHandler } from './../events-handlers/event-handler';

export class UserContainer{
	onlineUsers: Room = new Map(); // Online users is one big room
	rooms: Rooms = new Map(); //roomname: connectionid: connection

	constructor(ev: SocketEventHandler){
		ev.connection$.subscribe((conn: Connection) => this.addUser(conn));
		ev.close$.subscribe((conn: Connection) => this.onConnectionClose(conn));
	}

	//adds user to list of active users
	private addUser(conn: Connection){
		conn.rooms = new Map();		
		this.onlineUsers.set(conn.connectionID, conn);
		log.debug(`${this.onlineUsers.size} active users`)
	}

	//removes user from list of active users
	private removeUser(conn: Connection){
		this.onlineUsers.delete(conn.connectionID);
		log.debug(`${this.onlineUsers.size} active users`)
	}

	addUserToRoom(roomname: string, conn: Connection){
		let room = this.rooms.get(roomname);
		// create new room if not exist
		if(room === undefined){
			room = new Map();
			this.rooms.set(roomname, room);
		}
		room.set(conn.connectionID, conn);
		// add it on user connection
		(<Rooms> conn.rooms).set(roomname, room);
		log.debug(` adding user to room ${roomname}, ${room.size} in room`)	;		
	}

	removeUserFromRoom(roomname: string, conn: Connection){
		const room = this.rooms.get(roomname);
		if(room){
			room.delete(conn.connectionID);
			log.debug(`removing user from room ${roomname}, ${room.size} in room`);		
			// remove room if empty
			if(room.size < 1)
				this.rooms.delete(roomname);
		}
		// remove from ser connection too
		(<Rooms> conn.rooms).delete(roomname);
	}

	private onConnectionClose(conn: Connection){
		// user disconnects, thus leaving rooms he joined
		(<Rooms> conn.rooms).forEach((v, k) => this.removeUserFromRoom(k, conn));
		this.removeUser(conn);
	}
}

