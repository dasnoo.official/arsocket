import { Observable } from 'rxjs/Observable';
import { log, setLoggerLevel } from './../utils/logger';
import { UserContainer } from './../user-container/user-container';
import { Bridge } from './../bridges/bridge';
import { Router } from './../router/router';
import { Route } from './../router/route.interface';
import { SocketEventHandler } from '../events-handlers/event-handler';
import { Connection } from '../events-handlers/connection.interface';
import { OptionBuilder } from '../options/options-builder';
import { SocketOpts } from '../options/socket-opts.interface';
import { Printer } from "../utils/printer";
import { getHttpServer } from "../server/server";
import { Action, Reaction } from "@dasnoo/action-reaction";
import { Messenger } from "../messenger/mesenger";
import { ActionEvent } from "../index";
import { ISocket } from "./socket.interface";

export class Socket implements ISocket{
	private opts: SocketOpts;
	private router: Router;
	private server: any;
	private bridge: Bridge;

	public socket: any;	
	public eventHandler: SocketEventHandler;
	public userContainer: UserContainer;
	public messenger: Messenger;

	constructor(opts: SocketOpts = {}) {
		this.opts = OptionBuilder.build(opts);
		setLoggerLevel(this.opts.logLevel);
		this.bridge = new Bridge(this.opts.engine);
		this.createServer();
		this.eventHandler = new SocketEventHandler(this.bridge);
		this.userContainer = new UserContainer(this.eventHandler);
		this.messenger = new Messenger(this.userContainer, this.bridge);
	}

	private createServer(){
		this.server = getHttpServer(this.opts);
		this.socket = this.bridge.createServer(this.opts);
		setTimeout(_ => Printer.printInfos(this.opts), 100);
	}

	addRoutes(routerConfig: Array<Route>): Socket{
		if(routerConfig)
			this.router = new Router(this.eventHandler, routerConfig);
		setTimeout(_ => Printer.printRoutes(routerConfig), 110);
		return this;
	}

	send(connection: Connection, reaction: Reaction<any>): Socket{
		this.messenger.send(connection, reaction);
		return this;
	}

	broadcast(reaction: Reaction<any>, omit?: Connection, roomname?: string): Socket{
		this.messenger.broadcast(reaction, omit, roomname);
		return this;
	}

	// addroutes should be used instead, this is for convenience
	select(type: string): Observable<{payload: any, connection: Connection}>{
		// This is not the method that is supposed to be used for routing
		// just here for convenience and mimic the front end api, ence the "quick" and easy filter
		return this.eventHandler
			.action$
			.filter(a => a.action.type === type)
			.map(a => ( { payload: a.action.payload, connection: a.connection } ));
	}

	addUserToRoom(roomname: string, conn: Connection): Socket{
		this.userContainer.addUserToRoom(roomname, conn);
		return this;
	}
	removeUserFromRoom(roomname: string, conn: Connection): Socket{
		this.userContainer.removeUserFromRoom(roomname, conn);
		return this;
	}
}




