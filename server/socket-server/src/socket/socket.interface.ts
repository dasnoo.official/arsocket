import { Socket } from './socket';
import { Observable } from 'rxjs/Observable';
import { Route, Connection, UserContainer } from "../index";
import { Reaction } from "@dasnoo/action-reaction";

export interface ISocket {
	readonly socket: any;
	readonly userContainer: UserContainer;

    addRoutes: (routerConfig: Array<Route>) => Socket;
    // sends reaction to one specific connection.
    send: (connection: Connection, reaction: Reaction<any>) => Socket;
    // sends reaction to all with omition of the specified by omit. Only in room if specified
	broadcast: (reaction: Reaction<any>, omit?: Connection, roomname?: string) => Socket;
	// addroutes should be used instead, this is for convenience
    select: (type: string) => Observable<{payload: any, connection: Connection}>;
    
    addUserToRoom: (roomname: string, conn: Connection) => Socket;
	removeUserFromRoom: (roomname: string, conn: Connection) => Socket
}
