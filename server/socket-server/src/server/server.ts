import { log } from '../utils/logger';
import { SocketOpts } from '../options/socket-opts.interface';
import { createServer } from 'http';
import * as express from 'express';


export function getHttpServer(opts: SocketOpts):any{
	if(checkExisting(opts))
		return opts.server;
	const app = express();

	app.use((error: any, req: any, res: any, next: any) => {
			log.error(error);
			next();
	});

	const server = createServer(app);

	if(opts.staticDir)
		app.use('/', express.static(opts.staticDir));

	server.listen(opts.port, (err: any) => {
		if(err)
			log.error(err)
	});

	opts.server = server;
	return server;
}

// check if user provided a server and changes options accordingly
function checkExisting(opts: SocketOpts){
	if(opts.server){
		opts.customServer = true;
		// since http.listen is async if the server fails to start 
		// the error that caused the fail might be thrown after
		// and the address will be undefined. 
		const address = opts.server.address();
		opts.port = address ? address.port : undefined;
		// We cannot know if the static dir of a server that's not ours
		delete opts.staticDir;
		return true;
	}
	return false;
}


