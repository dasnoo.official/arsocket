import { SocketOpts } from './socket-opts.interface';
import { Engines } from "./engines.enum";

export const DEFAULT_OPTS: SocketOpts = {
		path: 'arsocket',
		logLevel: 'debug',
		port: +(process.env.PORT || 3000),
		staticDir: '',
		customServer: false,
		engine: Engines.uws
};