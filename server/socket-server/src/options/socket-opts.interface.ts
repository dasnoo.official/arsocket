
export interface SocketOpts {
	server?: any;
	path?: string;
	logLevel?: string,
	port?: number;
	customServer?: boolean,
	staticDir?: string,
	[key: string]: any;
}
