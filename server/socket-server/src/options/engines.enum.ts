export enum Engines{
	uws = 'uws',
	engineIO = "engine.io",
	socketIO = "socket.io",
	sockjs = "sockjs"
}