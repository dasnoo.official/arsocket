import { DEFAULT_OPTS } from './default_options';
import { SocketOpts } from "./socket-opts.interface";

// responsible to guarantee that socket will start whatever option is passed
// into Socket (to a certain extent).
export class OptionBuilder {

	static build(opts:SocketOpts = {}){
		if(opts.path)
			opts.path = OptionBuilder.formatPath(opts.path);
		const temp = Object.assign({}, DEFAULT_OPTS);
		return Object.assign(temp, opts);
	}

	// format leading space and /, same for end of str
	private static formatPath(str: string){
		return str.replace(/^[\s/]*|[\s/]*$/g, '');
	}
}

