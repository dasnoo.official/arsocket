import { Connection } from './../events-handlers/connection.interface';
import { Middleware } from "./middleware.interface";

export interface Route{
    type:string;
    handler: (payload?: any, connection?: Connection) => any;
    handlerWrapper?: Function;
    middlewares?: Array<Middleware>;
}
