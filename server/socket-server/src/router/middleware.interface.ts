import { ActionEvent } from './../events-handlers/action-event.interface';

export interface Middleware{
	(actionEvt: ActionEvent): boolean | Promise<boolean>;
}