import { Action } from '@dasnoo/action-reaction';
import { ActionEvent } from './../events-handlers/action-event.interface';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Bridge } from './../bridges/bridge';
import { log } from './../utils/logger';
import { Connection } from './../events-handlers/connection.interface';
import { Route } from './route.interface';
import { SocketEventHandler } from './../events-handlers/event-handler';
import { Reaction } from "@dasnoo/action-reaction";
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import { Middleware } from "./middleware.interface";
import { MWRouter } from "./middleware-router";

export class Router{
	//maps routes
	private routes : {[key: string]: Route} = {};
	private middlewares: Array<Middleware> = [];

	constructor(private evtHandler: SocketEventHandler, private routeConfig: Array<Route> ){
		this.addRoutes(routeConfig)
		evtHandler.action$.subscribe(a => this.route(a));
	}

	private route(a: ActionEvent){
		const r = this.routes[a.action.type];
		if(r && r.handlerWrapper)
			r.handlerWrapper(a);
		else{
			this.sendError(a, `route ${a.action.type} not added yet`);
			log.warn(`route ${a.action.type} not added yet`);
		}
	}

	private addRoutes(routeConfig: Array<Route>){
		routeConfig.forEach(r => this.addRoute(r));
	}

	private addRoute(route: Route){
		route.handlerWrapper = this.makeWrapper(route);
		//adding to map for easy access
		this.routes[route.type] = route;
	}

	private makeWrapper(route: Route): Function{
		return async (a: ActionEvent) => {
			try{
				// applying router middlewares first
				await MWRouter.applyMiddlewares(a, this.middlewares);
				// then route specific middlewares
				await MWRouter.applyMiddlewares(a, route.middlewares);
				// then handler
				const reaction = await route.handler(a.action.payload, a.connection);
				// if the response is a promise we need to react as well
				// resolve deals with that
				this.react(reaction, a);
			}catch(e){
				log.debug(e); 
			}
		};
	}

	// sends reaction message
	// giving the same id as the action.
	private react(r: any, actionEvt: ActionEvent, type?: string){
		if(r === undefined) return;
		const t = type || actionEvt.action.type;
		const id = actionEvt.action.id;
		const payload = r;
		const reaction: Reaction<any> = {type: t, id, payload};
		this.evtHandler.addReaction(reaction, actionEvt.connection);
	}

	private sendError( actionEvt: ActionEvent, msg: string){
		const origin = actionEvt.action.type;
		const error = { msg, origin }
		this.react(error, actionEvt, 'ERROR');
	}

}