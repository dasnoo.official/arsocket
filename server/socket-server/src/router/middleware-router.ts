import { Middleware } from './middleware.interface';
import { ActionEvent } from "../index";

export class MiddlewareRouter {

		// applying middlewares, if any middleware returns undefined we return undefined
		static applyMiddlewares(actionEvent: ActionEvent, middlewares: Array<Middleware> = []): Promise<any>{
			const promises = middlewares.map(m => this.applyMiddleware(m, actionEvent))
			return Promise.all(promises);
		}
	
		static applyMiddleware(m: Middleware, actionEvent: ActionEvent): Promise<any>{
			return new Promise( async (resolve, reject) => {
				const result = await m(actionEvent);
				if(!result)
					reject(`middleware ${m.name} returned false / undefined, forgetting message`);
				resolve(result);
			});
		}
}

export const MWRouter = MiddlewareRouter;
