import { Route } from './../router/route.interface';
import { SocketOpts } from './../options/socket-opts.interface';
const Table = require('cli-table2');

import 'colors';

// this class is ugly, hopefully it will get refactored..
export class Printer{

	static printInfos(opts: SocketOpts){
		Printer.printEnv();
		Printer.printLogo(opts);
		Printer.printOpts(opts);
	}

	static printEnv(){
		let env = process.env.NODE_ENV;
		console.log(`NODE_ENV: app running in ${env ? env : 'dev'} mode. `.blue
		+ `Set your NODE_ENV environment variable to production for production mode`.blue);
	}

	static printLogo(opts: SocketOpts){
		const info = `AR Socket listenning on 127.0.0.1:${opts.port}/${opts.path}`
		console.log(`${start_rocket.blue}    ${info.yellow}${end_rocket.blue}`);
	}

	static printOpts(opts: SocketOpts){
		const values = Printer.formatOpts(opts);
		console.log("\n Options:")
		Printer.printTable({
			head: ["Option".blue, "value".yellow], 
			colWidths: [20,50], 
			values
		});
	}

	static formatOpts(opts: SocketOpts){
		const values: Array<any> = [];
		Object.keys(opts).forEach(k => {
			let v = opts[k]
			if( typeof v === "string" && v.length > 40)
				opts[k] = Printer.formatLongString(v, 40);
			if( typeof v !== "object" && typeof v !== "function")
				values.push([k, opts[k]]);
		});
		return values;
	}

	static printRoutes(routerConfig: Array<Route>){
		const head = ['route type'.blue, 'handler'.yellow, 'middlewares'.yellow];
		const colWidths = [20,20, 30];
		const values: Array<any> = [];
		routerConfig.forEach(r => {
			if(r.middlewares){
				const middlewares = r.middlewares.map(m => m.name);
				values.push([r.type, r.handler.name, middlewares.toString()]);
			}else{
				values.push([r.type, r.handler.name, '']);
			}
		});
		console.log("Routes: ")
		Printer.printTable({ head, colWidths, values });
	}

	// put a return at pos long strings that don't fit the table
	static formatLongString(str: string, pos: number){
		return [str.slice(0, pos), '\n', str.slice(pos)].join('')
	}
	

	static printTable(t:ITable){
		const table = new Table({ head: t.head, colWidths: t.colWidths });
		table.push(...t.values);
		console.log(table.toString());
	}
}

const start_rocket = `
      /\\
     (  )`;
const end_rocket = `
     (  )
    /|/\\|\\
   /_||||_\\.`;
export interface ITable {
	head: Array<string>;
	colWidths?: Array<number>;
	values: Array<Array<string>>;
}