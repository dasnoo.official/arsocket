import { Action } from '@dasnoo/action-reaction';
import { SocketEventHandler } from "../index";

export class AntiSpam{
	// this might go away
	// naive method to counter send messages via the console from the client
	// justs check if the hash of the time stamp are equal.
	static isSpam(action:Action<any>){
		let r =  (action.hash !== AntiSpam.hash(action.timestamp));
		return r;
	}

	private static hash(timestamp = 0){
		let str = "!" + timestamp;
		let hash = 0;
    let length = str.length;
    if (length === 0) return hash;
    for (let i = 0; i < length; i++) {
      let char = str.charCodeAt(i);
      hash = ((hash<<5)-hash)+char;
      hash = hash & hash; 
    }
    return hash;
	}
}