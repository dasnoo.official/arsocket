import { 
	addColors, 
	Logger, 
	LoggerInstance, 
	transports, 
	AbstractConfigSetColors } from "winston";

const colors: AbstractConfigSetColors = {
	debug: 'grey',
	info: 'green',
	warn: 'yellow',
	error: 'red'
};

const levels = {
	debug: 4,
	info: 3,
	warn: 2,
	error: 1,
	test: 0
};
const level = 'debug';

export const log: LoggerInstance = new Logger({
	transports: [ new transports.Console({ colorize: true }) ],
	levels,
	level
});


export function setLoggerLevel(level: string = 'debug'){
	log.level = level;
}


addColors(colors);


