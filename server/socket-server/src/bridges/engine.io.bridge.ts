import { DEFAULT_OPTS } from './../options/default_options';
import { IBridge } from './bridge.interface';
import { Reaction } from "@dasnoo/action-reaction";
import * as Engine from 'engine.io';
import { SocketOpts } from "../index";

export class EngineIOBridge implements IBridge{
	socket: any;

	onConnection( fn: Function){
		this.socket.on('connection', fn);
	}

	onError(connection: any, fn: Function){
		connection.on('error', fn);
	}

	onAction(connection: any, fn: Function){
		connection.on('message', fn);
	}

	onClose(connection: any, fn: Function){
		connection.on('close', fn);
	}

	send(connection: any, reaction: Reaction<any>){
		connection.send(reaction);
	}

	createServer(httpServer: any, options: SocketOpts){
		let path = `/${options.path}`
		this.socket = Engine.attach(httpServer, { path });
		return this.socket;
	}
	
}