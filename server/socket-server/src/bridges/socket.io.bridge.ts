import { DEFAULT_OPTS } from './../options/default_options';
import { SocketOpts } from '../options/socket-opts.interface';
import { Reaction } from '@dasnoo/action-reaction';
import { IBridge } from './bridge.interface';
import * as Io from "socket.io";

export class SocketIOBridge implements IBridge{
	socket: any;
	onConnection(fn: Function){
		this.socket.on('connection', fn);
	}

	onError(connection: any, fn: Function){
		connection.on('error', fn);
	}

	onAction(connection: any, fn: Function){
		// socket.io can receive multiple events. We will send
		// only action event and make the routing ourselves.
		connection.on('', fn);
	}

	onClose(connection: any, fn: Function){
		connection.on('disconnect', fn);
	}

	send(connection: any, reaction: Reaction<any>){
		connection.emit('', reaction);
	}

	createServer(httpServer: any, options: any){
		this.socket = Io(httpServer, options);
		return this.socket;
	}

	getDefaultOptions(): SocketOpts{
		return DEFAULT_OPTS;
	}
}