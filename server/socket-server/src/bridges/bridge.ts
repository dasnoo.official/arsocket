import { UwsBridge } from './uws.bridge';
import { EngineIOBridge } from './engine.io.bridge';
import { SockJSBridge } from './sockjs.bridge';
import { createServer } from 'http';
import { IBridge } from './bridge.interface';
import { SocketIOBridge } from './socket.io.bridge';
import { Reaction } from "@dasnoo/action-reaction";
import { Connection } from "../events-handlers/connection.interface";
import { SocketOpts } from "../options/socket-opts.interface";

// sole purpose of this class is to decouple the rest of the app
// from any websocket implementation
export class Bridge{
	bridge: any;

	constructor(engine: string){
		this.setEngine(engine);
	}

	setEngine(engine: string){
		switch(engine){
			case "socket.io":
				this.bridge = new SocketIOBridge(); break;
			case "sockjs" :
				this.bridge = new SockJSBridge(); break;
			case "websocket":
				throw Error('not valid'); 
			case "engine.io":
				this.bridge = new EngineIOBridge(); break;
			case 'uws':
				this.bridge = new UwsBridge(); break;
			default:
				throw Error(`${engine} hasn't been implemented yet.`);
		}
	}

	onConnection(fn: Function){
		return this.bridge.onConnection(fn);
	}

	onError(connection: Connection, fn: Function){
		return this.bridge.onError(connection.engineConnection, fn);
	}

	onAction(connection: Connection, fn: Function){
		return this.bridge.onAction(connection.engineConnection, fn);
	}

	onClose(connection: Connection, fn: Function){
		return this.bridge.onClose(connection.engineConnection, fn);
	}

	send(connection: Connection, reaction: Reaction<any>){
		return this.bridge.send(connection.engineConnection, reaction)
	}

	createServer(options: SocketOpts){
		return this.bridge.createServer(options.server, options)
	}

	getDefaultOptions(){
		return this.bridge.getDefaultOptions();
	}
}