import { SockJSBridge } from './sockjs.bridge';
import { DEFAULT_OPTS } from './../options/default_options';
import { SocketOpts } from './../options/socket-opts.interface';
import { Reaction } from "@dasnoo/action-reaction";
import * as Uws from 'uws';

export class UwsBridge extends SockJSBridge{
	socket: any;

	createServer(httpServer: any, options: any){
		const uwsoptions: any  = {};
		uwsoptions.server = httpServer;
		uwsoptions.path = options.path;
		this.socket = new Uws.Server(uwsoptions);
		return this.socket;
	}

	onAction(connection: any, fn: Function){
		connection.on('message', fn);
	}
	
	send(connection: any, reaction: Reaction<any>){
		connection.send(JSON.stringify(reaction));
	}
}
