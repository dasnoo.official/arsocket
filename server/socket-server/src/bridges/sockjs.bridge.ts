import { IBridge } from './bridge.interface';
import { DEFAULT_OPTS } from './../options/default_options';
import { SocketOpts } from './../options/socket-opts.interface';
import { Reaction } from "@dasnoo/action-reaction";
import * as SockJS from 'sockjs';


export class SockJSBridge implements IBridge {
	socket: any;

	onConnection(fn: Function){
		this.socket.on('connection', fn);
	}

	onError(connection: any, fn: Function){
		connection.on('error', fn);
	}

	onAction(connection: any, fn: Function){
		connection.on('data', fn);
	}

	onClose(connection: any, fn: Function){
		connection.on('close', fn);
	}

	send(connection: any, reaction: Reaction<any>){
		connection.write(JSON.stringify(reaction));
	}

	createServer(httpServer: any, options: any){
		const sockjsOpts: any = {}
		// removing logging from sockjs
		sockjsOpts.log = (_: any) => {};
		sockjsOpts.prefix = `/${options.path}`;
		this.socket = SockJS.createServer({ sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js' });
		this.socket.installHandlers(httpServer, sockjsOpts);
		return this.socket;
	}

}
