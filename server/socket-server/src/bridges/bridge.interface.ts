import { Reaction } from "@dasnoo/action-reaction";
import { SocketOpts } from "../index";

export interface IBridge {

	onConnection: (fn: Function) => any;

	onError: (engineConnection: any, fn: Function) => any;

	onAction: (engineConnection: any, fn: Function) => any;

	onClose: (engineConnection: any, fn: Function) => any;

	send: (engineConnection: any, reaction: Reaction<any>) => any;

	createServer: (httpServer: any, options: SocketOpts) => any;

}