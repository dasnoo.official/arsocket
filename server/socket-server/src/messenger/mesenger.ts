import { Reaction } from '@dasnoo/action-reaction';
import { Bridge } from "../bridges/bridge";
import { Connection } from "../events-handlers/connection.interface";
import { UserContainer } from "../user-container/user-container";

export class Messenger {

	constructor(private userContainer: UserContainer, private bridge: Bridge){
	}

	send(connection: Connection, reaction: Reaction<any>){
		this.bridge.send(connection, reaction);
	}

	broadcast(reaction: Reaction<any>, omit?: Connection, roomname?: string){
		let map;
		if(roomname === undefined)
			map = this.userContainer.onlineUsers;
		else
			map = this.userContainer.rooms.get(roomname);

		if(!map) return;

		map.forEach(conn => {
			if(!omit || conn.connectionID !== omit.connectionID)
				this.bridge.send(conn, reaction);
		});
	}
}