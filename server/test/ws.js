"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dist_1 = require("../socket-server/dist");
const socket = new dist_1.Socket({ staticDir: '../../client/test', engine: 'uws' });
const routes = [
    { type: "NEW_MESSAGE", handler: onMessage },
    { type: "JOIN", handler: onJoin, middlewares: [authorize] }
];
socket.addRoutes(routes);
function onMessage(payload, connection) {
    console.log('onmessage');
    //socket.broadcast({type:"NEW_MESSAGE", payload: "client says " + payload}, undefined, "room1");
    return payload;
}
function onJoin(payload, c) {
    socket.userContainer.addUserToRoom("room1", c);
}
function authorize({ connection, action }) {
    console.log('authorize');
    return true;
}
