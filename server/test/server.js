"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const http_1 = require("http");
const path = require("path");
const app = express();
exports.server = http_1.createServer(app);
//cors
exports.server.listen(3004);
let sdir = path.join(__dirname + '/../../client/test');
app.use('/', express.static(sdir));
console.log(sdir);
