import { Socket, Route, Connection } from "../socket-server/dist";

const socket = new Socket({staticDir: '../../client/test',  engine : 'uws'});


const routes: Array<Route> = [
  { type: "NEW_MESSAGE", handler: onMessage},
  { type: "JOIN", handler: onJoin, middlewares: [authorize]}
];

socket.addRoutes(routes)

function onMessage(payload, connection: Connection){
  console.log('onmessage')

  //socket.broadcast({type:"NEW_MESSAGE", payload: "client says " + payload}, undefined, "room1");
  return payload;
}
function onJoin(payload, c){
  socket.userContainer.addUserToRoom("room1", c);
}

function authorize({connection, action}): boolean{
  console.log('authorize')
  return true;
}
